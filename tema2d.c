#include <stdlib.h>
#include "utils.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/time.h>

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define CASE 1

int main(int argc, char **argv)
{
    double tp;
    struct timeval start, stop;
    
    struct test **tests;
    struct test *tests_to;
    tests = (struct test **)malloc(sizeof(struct test*));
    (*tests) = (struct test *)calloc(MAXTESTS, sizeof(struct test));

    double *A;
    double *B;
    double *C;

    FILE *file_A, *file_B, *file_C;
    int j;

    if (argc != 2)
        error("Usage: tema2 <cfg_file>");
    
    parse_config(argv[1], tests);
    tests_to = *tests;

    int t, i, k;

    int ii, kk;
    for (t = 0; t < MAXTESTS; t++) {
    	gettimeofday(&start,0);
        if (tests_to[t].name[0] == '\0')
            break;
        struct test my_test = tests_to[t];
        printf("%i. Test[%s] \'known\' as %s:\n", t, my_test.name, my_test.test_name);

      	int N = my_test.N;
        int M = my_test.M;
        int K = my_test.K;
        double alpha = my_test.alpha;
        double beta = my_test.beta;

        /*alocare matrici*/
        A = malloc(sizeof(double)*M*K);
        B = malloc(sizeof(double)*K*N);
        C = malloc(sizeof(double)*M*N);

        char in_A[80];
        strcpy(in_A,"input/");
        strcat(in_A, my_test.name);
        strcat(in_A, "_A.in");

        char in_B[80];
        strcpy(in_B,"input/");
        strcat(in_B, my_test.name);
        strcat(in_B, "_B.in");

        char in_C[80];
        strcpy(in_C,"input/");
        strcat(in_C, my_test.name);
        strcat(in_C, "_C.in");
        

        file_A = fopen(in_A, "r");
        if(file_A < 0)
            printf("open error\n");

        file_B = fopen(in_B, "r");
        if(file_B < 0)
            printf("open error\n");

        file_C = fopen(in_C, "r");
        if(file_C < 0)
            printf("open error\n");

        /*citire din fisiere*/

        if(my_test.transa == 'T'){

            for (j = 0; j < M; j++)
                for(register int k = 0; k < K; k++)
                {
                    fscanf(file_A, "%lf ", &A[k*K + j]);
                }
        }
        else{
            for (j = 0; j < M; j++)
                for(register int k = 0; k < K; k++)
                {
                    fscanf(file_A, "%lf ", &A[j*K + k]);
                }
        }

        if(my_test.transb == 'T'){
            for (j = 0; j < K; j++)
                for(register int k = 0; k < N; k++)
                {
                    fscanf(file_B, "%lf ", &B[k*N + j]);
                }
        }
        else{
            for (j = 0; j < K; j++)
                for(register int k = 0; k < N; k++)
                {
                    fscanf(file_B, "%lf ", &B[j*N + k]);
                }
        }


        for (j = 0; j < M; j++)
            for(register int k = 0; k < N; k++)
            {
                fscanf(file_C, "%lf ", &C[j*N + k]);
            }
        
    switch(CASE)
    {
    case 1:
    {
        /*Blocked Matrix Multiplication folosind bloc de dimensiune 128*/
        int bb = 128;

        for (j = 0; j < M; j++)
            for(register int k = 0; k < N; k++)
            {
                C[j*N + k] *= beta;
            }

        for (ii = 0; ii < N; ii += bb) {
            for (kk = 0; kk < K; kk += bb) {
                for (i = 0; i < M; i++) {
                    for (j = ii; j < MIN(ii + bb, N); j++) {
                        register double sum = 0.00000;
                        for (k = kk; k < MIN(kk + bb, K); k++) {
                            sum += alpha * *(A + i * K + k) * *(B + k * N + j);
                        }
                        *(C + i * N + j) += sum;
                    }
                }
            }
        }
        break;
    }

    case 2:
    {
        /* 	imultirea pe linie, presupune ca matricea B sa fie transpusa
        *	rezultatele acestei metode sunt comparabile cu cele returnate
        *	de Blocked Matrix Multiplication*/
        
        double *backup_C = C;
        double *backup_A = A;
        double *backup_B = B;

		int j,p;
        for (j = 0; j < M; j++)
        {
            double *backup_A1 = NULL;
            double *backup_B1 = backup_B;

            for (p = 0; p < N; p++)
            {
                backup_A1 = backup_A;

                register double sum = 0.000;
                for (register int k = 0; k < K; k++)
                {
                    sum += (*backup_A1) * (*backup_B1);
                    backup_A1++;
                    backup_B1++;
                }

                *backup_C = beta * (*backup_C) + alpha * sum;
                backup_C++;
            }

            backup_A = backup_A1;
        }
        break;
    }

    case 3:
        /*inmultire triviala*/
        for (i = 0; i < N; i++){
			for (j = 0; j < N; j++){
				C[i* N + j] = 0.0;
				for (k = 0; k < N; k++){
					C[i * N + j] += alpha * A[i * K + k] * B[k * N + j] + beta * C[i * N + j];
				}
			}
		}
		break;
	}

        char out[80];
        strcpy(out,"out/");
        strcat(out, my_test.name);
        strcat(out, ".out");

        FILE *f_out = fopen(out, "wb");
        if (f_out == NULL)
        {
            printf("Error opening file!\n");
            exit(1);
        }

        for(j = 0; j < M; j++){
            for(k = 0; k < N; k++){
                fprintf(f_out, "%.3f ", C[j*N + k]);
            }
            fprintf(f_out, "\n");
        }

        fclose(file_A);
        fclose(file_B);
        fclose(file_C);
        fclose(f_out);
        gettimeofday(&stop,0);
	    tp = (stop.tv_sec - start.tv_sec) + (double)(stop.tv_usec - start.tv_usec) / 1000000.0;
	    printf("Timp optimizare = %lf\n", tp);

    }

    gettimeofday(&stop,0);
    tp = (stop.tv_sec - start.tv_sec) + (double)(stop.tv_usec - start.tv_usec) / 1000000.0;
    printf("Timp optimizare = %lf\n", tp);
    return 0;
}

