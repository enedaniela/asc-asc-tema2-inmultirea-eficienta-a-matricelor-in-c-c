build: tema2d.c utils.c utils.h
	gcc -O1 -funroll-all-loops -Wall -o tema2 utils.c tema2d.c
run: build
	./tema2 tema2.cfg
verbose: tema2d.c utils.c utils.h
	gcc -O1 -D VERBOSE -Wall -o tema2 utils.c tema2d.c

clean:
	-rm -rf tema2d a.out
